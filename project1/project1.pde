/////////////////////////////////////////////////////////////////////////////
/*
Joseph Bauer
CS 6260 Fall 2018

Boston Housing Data

This script makes a scatter plot to try and help the user better understand the data

On setup, all of the calculations are done in the setup function. These calculations determine the values of the ticks in the X and y axis and
the postion of each node based on the current xAxisLabel and yAxisLabel. Whenever the mouse button is pressed, randomXandY() is called and will randomly determine a 
new xAxisLabel and yAxisLabel. This function will then clear the screen and redo all of the calculations based off of the new labels.

The draw function uses all of the calculations in the setup and randomXandY functions to constatnly draw the data to the screen.

*/
/////////////////////////////////////////////////////////////////////////////


// Node Object
// This object is used to store the data for each row in the given CSV file.
// All of the variables, except for x and y postion variables, are a header in the data file.
// A node is created for each row in the csv file.
class Node {

    // Variables
    public float crime; // per capita crime rate by town
    public float zn;    // proportion of residential land zoned for lots over 25,000 sq ft
    public float indus; // Proportion of non-retail business acres per town
    public float chas;    // 1 if bound to Charles river, 0 otherwise
    public float nox;   // nitrogen oxide concentration
    public float rm;
    public float age;
    public float dis;
    public float rad;
    public float tax;
    public float ptratio;
    public float black;
    public float lstat;
    public float medv;
    public float x_pos;
    public float y_pos;

    // Constructor
    Node(TableRow row){
        crime = row.getFloat("CRIM");
        zn = row.getFloat("ZN");
        indus = row.getFloat("INDUS");
        chas = row.getInt("CHAS");
        nox = row.getFloat("NOX");
        rm = row.getFloat("RM");
        age = row.getFloat("AGE");
        dis = row.getFloat("DIS");
        rad = row.getFloat("RAD");
        tax = row.getFloat("TAX");
        ptratio = row.getFloat("PTRATIO");
        black = row.getFloat("BLACK");
        lstat = row.getFloat("LSTAT");
        medv = row.getFloat("MEDV");
    }

    // Takes a string. This functin can receive any element of the global variable array.
    // Will return the value of the given column of a node
    public float getValue(String input){
        float toReturn = 0.0;

        switch(input){
            case "crime":
                toReturn = crime;
                break;
            case "zone":
                toReturn = zn;
                break;
            case "indus":
                toReturn = indus;
                break;
            case "chas":
                toReturn = chas;
                break;
            case "nox":
                toReturn = nox;
                break;
            case "rm":
                toReturn = rm;
                break;
            case "age":
                toReturn = age;
                break;
            case "dis":
                toReturn = dis;
                break;
            case "rad":
                toReturn = rad;
                break;
            case "tax":
                toReturn = tax;
                break;
            case "ptratio":
                toReturn = ptratio;
                break;
            case "black":
                toReturn = black;
                break;
            case "lstat":
                toReturn = lstat;
                break;
            case "medv":
                toReturn = medv;
                break;
        } 

        return toReturn;
    }


    // Both of the following function are used to find exactly where the node is supposed to fall given the current x and y axis labels
    void calculateXPos(String xLabel){
        float lowestValue = xAxisTicks[0];                 
        float highestValue = xAxisTicks[9];                 

        float difference = highestValue - lowestValue;

        float thisNodeValue = this.getValue(xLabel) - lowestValue;
        float percentage = (thisNodeValue / difference);
        
        float pixelValue = (1000 * percentage) + 150;

        this.x_pos = pixelValue;
    }

    void calculateYPos(String yLabel){
        float lowestValue = yAxisTicks[0];                 
        float highestValue = yAxisTicks[9];                 

        float difference = highestValue - lowestValue;
        
        float thisNodeValue = this.getValue(yLabel) - lowestValue;
        
        float percentage = (thisNodeValue / difference);
        
        float pixelValue = (1000 * percentage);
        
        pixelValue = 1000 - pixelValue;

        this.y_pos = pixelValue;
    }

}

// Global Constants
static float GRAPH_BOTTOM_LEFT_X = 150;
static float GRAPH_BOTTOM_LEFT_Y = 1000;

static float GRAPH_BOTTOM_RIGHT_X = 1150;
static float GRAPH_BOTTOM_RIGHT_Y = 1000;

// All Headers from the CSV file
static String variables[] = {
    "crime",
    "zone",
    "indus",
    "chas",
    "nox",
    "rm",
    "age",
    "dis",
    "rad",
    "tax",
    "ptratio",
    "black",
    "lstat",
    "medv"
};

// Global Variables
Node[] nodes;

String xAxisLabel = variables[0];
String yAxisLabel = variables[1];

float[] xAxisTicks = new float[10];
float[] yAxisTicks = new float[10];

//////////////////////////////

void setup() {
    //size(1400, 1200);
    
    fullScreen();

    noSmooth();
    
    background(255);

    // Parse data from file
    loadData();

    // Set up the x and y axis
    line(150, 0, GRAPH_BOTTOM_LEFT_X, GRAPH_BOTTOM_LEFT_Y);   // Y-axis
    line(GRAPH_BOTTOM_LEFT_X, GRAPH_BOTTOM_LEFT_Y, GRAPH_BOTTOM_RIGHT_X, GRAPH_BOTTOM_RIGHT_Y); // X-axis

    // Calculate the initial deviation of the x and y axis labels
    calculateXAxisTicks();
    calculateYAxisTicks();
    calculateNodesPositions();

}

//////////////////////////////

void draw() {
    drawAxisTicks();
	
    // Draw nodes
    drawNodes(); 
    
    drawLabels();
    
    if(mousePressed == true){
      randomXandY();
    }
}
///////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////
void loadData(){
    Table dataTable = loadTable("data.csv", "header");
    
    nodes = new Node[dataTable.getRowCount()];
    
    for(int i = 0; i < dataTable.getRowCount(); i++)
    {
       nodes[i] = new Node(dataTable.getRow(i)); 
    }
}

// Calculate each tick for the X axis
void calculateXAxisTicks(){
  
    strokeWeight(2);

    float lowestValue = findLowestValueInNodes(xAxisLabel);
    float highestValue = findHighestValueInNodes(xAxisLabel);

    float difference = highestValue - lowestValue;

    float deviation = difference / 10.0;

    xAxisTicks[0] = lowestValue;

    for(int i = 1; i < 9; i++){
        xAxisTicks[i] = xAxisTicks[i -1] + deviation; 
    }

    xAxisTicks[9] = highestValue;

}

// Calculate each tick for the Y axis
void calculateYAxisTicks(){
    
    strokeWeight(2);

    float lowestValue = findLowestValueInNodes(yAxisLabel);
    float highestValue = findHighestValueInNodes(yAxisLabel);

    float difference = highestValue - lowestValue;

    float deviation = difference / 10.0;

    yAxisTicks[0] = lowestValue;

    for(int i = 1; i < 9; i++){
        yAxisTicks[i] = yAxisTicks[i -1] + deviation; 
    }

    yAxisTicks[9] = highestValue;
}

// After the ticks are calculated, this function will draw ticks 100 pixels apart
// With their correct label.
void drawAxisTicks(){

    strokeWeight(2);
    float increment = 0;
    textSize(18);
    fill(0);

    // Loop that draws the x axis ticks with labels on the chart
    for(int i = 0; i < xAxisTicks.length; i++){
        line(GRAPH_BOTTOM_LEFT_X + increment, GRAPH_BOTTOM_LEFT_Y - 10, GRAPH_BOTTOM_LEFT_X + increment, GRAPH_BOTTOM_LEFT_Y + 50); 

        text(str(xAxisTicks[i]), GRAPH_BOTTOM_LEFT_X + increment - 35, GRAPH_BOTTOM_LEFT_Y + 65);

        increment = increment + 100;
    }

    increment = 0;

    // Loop that draws the y axis ticks on the chart
    for(int i = 0; i < xAxisTicks.length; i++){
        line(GRAPH_BOTTOM_LEFT_X + 10, GRAPH_BOTTOM_LEFT_Y - increment, GRAPH_BOTTOM_LEFT_X - 50, GRAPH_BOTTOM_LEFT_Y - increment);                

        text(str(yAxisTicks[i]), GRAPH_BOTTOM_LEFT_X - 65, GRAPH_BOTTOM_LEFT_Y - increment);

        increment = increment + 100;

    }
}


// Takes a column from Nodes class and returns the lowest value in the nodes array
float findLowestValueInNodes(String input){
    float toReturn = nodes[0].getValue(input);

    for(int i = 0; i < nodes.length; i++){
         
        if(nodes[i].getValue(xAxisLabel) < toReturn){
            toReturn = nodes[i].getValue(xAxisLabel);
        }
    }

    return toReturn;
}

// Takes a column from Nodes class and returns the highest value in the nodes array
float findHighestValueInNodes(String input){
    float toReturn = nodes[0].getValue(input);

    for(int i = 0; i < nodes.length; i++){
         
        if(nodes[i].getValue(xAxisLabel) > toReturn){
            toReturn = nodes[i].getValue(xAxisLabel);
        }
    }

    return toReturn;
}

// Calls two functions on each node. Each function calculates the x and y
// position, based off of the glolbal variable for each label
void calculateNodesPositions(){
    for(int i = 0; i < nodes.length; i++){
        nodes[i].calculateXPos(xAxisLabel);
        nodes[i].calculateYPos(yAxisLabel);
    }
}

// Draws every node using their pre-calculated x and y position values
void drawNodes(){
    strokeWeight(5);
    for(int i = 0; i < nodes.length; i++){
        point(nodes[i].x_pos, nodes[i].y_pos);         
    }
}

// Draw the x and y axis labels
void drawLabels(){
  text(xAxisLabel, 0, 500);
  text(yAxisLabel, 1200, 1050);
}


// Finds a new X and Y axis randomly. Redo all calculations so that the draw function
// Can draw all of the new values
void randomXandY(){
   float randomX = 0;
   float randomY = 0;
   
   background(255);

   strokeWeight(1);
   line(150, 0, GRAPH_BOTTOM_LEFT_X, GRAPH_BOTTOM_LEFT_Y);   // X-axis
   line(GRAPH_BOTTOM_LEFT_X, GRAPH_BOTTOM_LEFT_Y, GRAPH_BOTTOM_RIGHT_X, GRAPH_BOTTOM_RIGHT_Y); // Y-axis
   
   while(randomX == randomY){
     randomX = random(13); 
     randomY = random(13);
   }
   
    xAxisLabel = variables[int(randomX)];
    yAxisLabel = variables[int(randomY)];
   
    calculateXAxisTicks();
    calculateYAxisTicks();
    calculateNodesPositions();

}
