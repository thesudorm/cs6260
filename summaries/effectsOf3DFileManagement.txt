.TL
3D or Not 3D? Summary
.AU
Joseph Bauer

.PP
Computers have become much more powerful over the years. Video games are a great example to showcase their ability to render things in 3D. However, 3D interfaces still seem to be a little too niche and have not seen many applications. There are a few programs that try to create a 3D interface for users, but there is a lack of research to determine the pros and cons of such an interface. This paper aims to answer two questions. First, what differences are there between working with 3D or 2D interfaces? Is one better than the other? Or are they both as effective? The second question is to determine the preferences of users. Do users prefer a 3D interface or a 2D? To answer these, this paper examaines previous work on the subject, describes two interfaces that are used in an experiment and explain how the experiment was designed. 

The author goes on to talk about a few papers that have done research in this field.
The first compared bookmark retrieval times between Data Mountain, a 3D interface for file management, and Internet Explorer.
They used a number of different cueing conditions to prompt the searches. The paper found that Data Mountain was more efficent and less prone to errors than IE. 
The study was not able to exactly explain what causes this increase in effectiveness.
Other studies show that users prefer using a 3D interface, but that there is little to gain from using a 3D interface. 

To gain their own insight, the author describes the experiment that they conducted to see if using a 2D or 3D interface is more effective. The task was to see if there were any difference between 2D or 3D in storing and retrieving web thumbnail images. They also wanted to see whether the density of the data affected retrieval times. 
The three data densities were sparse, medium and dense (33, 66 and 99 web pages respectively).
The experiment consisted of repeated tasks of storing webpages and retrieving random webpages. 

For the storage part of the experiment, there did not seem to be any significant difference in the amount of time it took to store the webpages. This held true for each of the densities. The author also goes into detail about some of the similar patterns that users had when storing the thumbnails, such as grouping pages with similar topics and creating subcatergories during the experiment. 
As for the retrieval tasks, the 3D interfaces mean time to complete the task was slightly lower than the 2D's. There were three main causes for the subjects to fail: Mis-catergorized pages, cross-catergorized pages and large catergories that have minimal separations between pages.  
When the author asked participants which they preferred, users chose the 3D interface.

Overall, there is no major difference in performance for using one interface over the other. The storage task showed no significant difference between the two interfaces. 

