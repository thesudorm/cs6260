# USEFUL JUNK

## Introduction
- Many experts critize the use of visual embellishment in charts and graphs
- "chart junk" can make interpretation more difficult
- minimalist perspective advocates plain charts that maximizes data ink
    - data ink: the ink in the chart used to re[resent data
- The Main Question: Do embellishments cause comprehension problems and do embellishments provide value to the reader.
- author conducted their own study that tested the effects of visual embellishments
    - Found no difference in interpretation accuracy
    - participants were able to remember the embellished charts longer
        sggests that people do notice and process the visual imagery and that it may help encode it for memory
- HOWEVER, imagery can bias the person reading the information. Use with caution

## Previous Work
#### Debate over Visual Embellishment
- Two sides, those that encourage embellishment and those that do not
- Tufte
    - Well knowm for being against visual embellishments
    - data-ink ratio should be as close to one as possible
        - data-ink ratio: Comparison of data ink vs all of the ink used in the graphic
    - minimalistic designs aim to reduce interpretation accuracy
- Holmes
    - Designer that uses a low data-ink ratio
    - states that graphics "must engage a reader"
        

