# MmatrixExplorer

## Introduction
- IV has been used for social network analysis since the 30
- Used to explore and communicate results
    - However, not that many social scientitst use IV
- Explore MatrixExplorer
    - node-link and matrix representation
    - tools for reorganizing, clustering and filtering graphs using a matrix

## Related Work (What are the requirements for social scientists?

- What is a social network?
    - structures made of human actors linked by relation
- Two catergories of visualization systems
    - Programming-based 
    - Menu-based

Programming-Based
    - highly tunable, versatile.
    - wide range of algorithms that can be used.
    - contain most of the effective and efficent alforithms to draw trees and graphs
    - However, they are generic and have no built in support for social network exploration
        - This makes it too difficult for most social scientists to use

Menu-based
    - provide a basic interface, no need to have programming knowledge
    - Have dozens of algorithms and statistical measures that answer all questions, but hard to know what questions to ask
    - Expert users are fine, but novices find this a lot of trial and error
    - not intuitive for primary exploration
        - Designed for anaylsis by an expert.

## Exploratory Analysis Requirements

Researchers focused on three specific questions
    1. How would you like to create a social network?
    2. How would you like to edit a created social network?
    3. How would you like to explore an unknown social network?

Session was made of 4 stages

1. Presented participants with state of the art tools for social network analysis and some broad techniques for for interacting for graphs and data. Did not give them any guidance

2. Split up participants in small groups and brainstormed ideas

3. Participants made paper prototypes of ideas and filming interating with them

4. reviewed ideas as a group and gathered common/important ideas.

#### Requirements found in the experiment

1. Multiple representation
    - Participants used both node-link and matrix representations
    - Node is prefered, but matrix was necessary for large/dense graphs

2. Connected Components
    - Real graphs contain several connected components
    - Needs to be able to handle this for navigation and comparison

3. Overview
    - Hard for large graphs 
    - important for exploratory process
    - help users build mental map of network

4. Dataset general information
    - Things like type of graph, # of vertices, edges, ect should be easy to access at all times

5. Attributes
    - able to build several representations according to different attributes for the edges and vertices

6. Analytical Info

7. Interation vs parameter tuning
    - Participants asked for more interaction with graphs and less parameters

8. Layout
    several layouts are needed to understand a graph

9. Filtering
    - Filter needs to be easy to see that it may be in effect

10. Clusters
    - Needed to handle multiple clusters in data and annotating them

11. Outliers
    Should not filter out outliers as dataset noise

12. Consensus
    - Tools to identify a consensus among the clusters or differences

13. Aggregation
    Have the ability to switch between aggragated data and the full data set. Information can be lost in the aggregated view

## Matrix Explorer

A first attempt to fulfill social sciences researchers requirements to explore data. These are some of the main features

#### Node link diagrams coupled with matrices

- Based on matrix and node-link diagrams
